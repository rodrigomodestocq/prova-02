package Br.ufc.Quixada.si.model;

import java.time.LocalDate;
public class Funcionario extends Pessoa {

	private String matricula;
	private LocalDate dataAdmissao;
	private double salario;

	public Funcionario(String nome, String cpf, LocalDate dataNasc, String matricula, LocalDate dataAdmissao,
			double salario) {
		super(nome, cpf, dataNasc);
		this.matricula = matricula;
		this.dataAdmissao = dataAdmissao;
		this.salario = salario;
	}


	public String getMatricula() {
		return matricula;
	}
	public LocalDate getDataAdmissao() {
		return dataAdmissao;
	}
	public double getSalario() {
		return salario;
	}

	public void mostrarFuncionario() {
		System.out.println(getNome() + " " + getCpf() + " " +
						   getDataNasc() + " " + getMatricula() + " " + 
						   getDataAdmissao() + " " + getSalario());

	}




}
