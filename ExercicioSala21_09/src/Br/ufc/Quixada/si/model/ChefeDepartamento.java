package Br.ufc.Quixada.si.model;
import java.time.LocalDate;

public class ChefeDepartamento extends Funcionario {

	private String departamento;
	private LocalDate dataPromocao;
	private double gratificacao;

	public ChefeDepartamento(String nome, String cpf, LocalDate dataNasc, String matricula,
			LocalDate dataAdmissao, double salario, String departamento, LocalDate dataPromocao,
			double gratificacao) {
		super(nome, cpf, dataNasc, matricula, dataAdmissao, salario);
		this.departamento = departamento;
		this.dataPromocao = dataPromocao;
		this.gratificacao = gratificacao;
	}



	public String getDepartamento() {
		return departamento;
	}
	public LocalDate getDataPromocao() {
		return dataPromocao;
	}
	public double getGratificacao() {
		return gratificacao;
	}

	public void mostrarChefe() {
		System.out.println(getNome() + " " + getCpf() + " " +
						   getDataNasc() + " " + getMatricula() + " " + 
						   getDataAdmissao() + " " + getSalario() + " " +
						   getDepartamento() + " " + getDataPromocao() + " " + getGratificacao());

	}


}
