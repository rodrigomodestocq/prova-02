package Br.ufc.Quixada.si.model;
import java.time.LocalDate;

public class Pessoa {
	private String nome;
	private String cpf;
	private LocalDate dataNasc;
	
	public Pessoa(String nome, String cpf, LocalDate dataNasc) {
		this.nome = nome;
		this.cpf = cpf;
		this.dataNasc = dataNasc;
	}
	
	
	public String getNome() {
		return nome;
	}
	public String getCpf() {
		return cpf;
	}
	public LocalDate getDataNasc() {
		return dataNasc;
	}
	
	public void mostrarPessoa() {
		System.out.println(getNome()+ " " + getCpf() + " " +getDataNasc());
		
	}

	
	
	
}
