package Br.ufc.Quixada.si.model;

import java.time.LocalDate;

public class Aluno extends Pessoa {
	private String matricula;

	public Aluno(String nome, String cpf, LocalDate dataNasc, String matricula) {
		super(nome, cpf, dataNasc);
		this.matricula = matricula;
	}

	public String getMatricula() {
		return matricula;
	}

	public void mostrarAluno() {
		System.out.println(getNome() + " " + getCpf() + " " +
						   getDataNasc() + " " + getMatricula());

	}


}
