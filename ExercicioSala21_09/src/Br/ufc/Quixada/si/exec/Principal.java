package Br.ufc.Quixada.si.exec;
import java.time.LocalDate;
import java.util.Scanner;

import Br.ufc.Quixada.si.model.Aluno;
import Br.ufc.Quixada.si.model.ChefeDepartamento;
import Br.ufc.Quixada.si.model.Funcionario;
import Br.ufc.Quixada.si.model.Pessoa;

public class Principal {

	public static void main(String[] args) {


		Scanner so = new Scanner(System.in);
		
		System.out.println("Digite uma data no formato aaaa-mm-dd: ");
		String aux = so.next();
		
		LocalDate d = LocalDate.parse(aux);
		
		LocalDate c = LocalDate.of(2018, 9, 21);
		
		Pessoa p1 = new Pessoa(null, null, d);
		Funcionario f1 = new Funcionario(null, null, null, null, null, 0);
		Aluno a1 = new Aluno(null, null, null, null);
		ChefeDepartamento cd1 = new ChefeDepartamento(null, null, null, null, null, 0, null, null, 0);

		p1.mostrarPessoa();
		f1.mostrarFuncionario();
		a1.mostrarAluno();
		cd1.mostrarChefe();
	}

}
