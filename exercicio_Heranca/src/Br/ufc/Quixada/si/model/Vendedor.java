package Br.ufc.Quixada.si.model;

public class Vendedor extends Empregado {
	private double valorVendar;
	private double comissao;
	public Vendedor() {
	}
	public Vendedor(String nome, String endereco, String telefone, int codigoSetor, double salarioBase, double imposto,
			double valorVendar, double comissao) {
		super(nome, endereco, telefone, codigoSetor, salarioBase, imposto);
		this.valorVendar = valorVendar;
		this.comissao = comissao;
	}
	public double getValorVendar() {
		return valorVendar;
	}
	public void setValorVendar(double valorVendar) {
		this.valorVendar = valorVendar;
	}
	public double getComissao() {
		return comissao;
	}
	public void setComissao(double comissao) {
		this.comissao = comissao;
	}
	
	@Override
	public void calcularSalario(double resultado) {
	
		super.calcularSalario(resultado+comissao);
	
	}
	@Override
	public String toString() {
		return "Vendedor [valorVendar=" + valorVendar + ", comissao=" + comissao + ", Nome=" + getNome()
				+ ", Endereco=" + getEndereco() + ", Telefone=" + getTelefone() + ", CodigoSetor="
				+ getCodigoSetor() + ", SalarioBase=" + getSalarioBase() + ", Imposto=" + getImposto() + "]";
	}
	
	
}
