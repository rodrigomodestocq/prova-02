package Br.ufc.Quixada.si.model;

public class Administrador extends Empregado{
	private double ajudaDeCusto;

	public Administrador() {
		
	}
	public Administrador(String nome, String endereco, String telefone, int codigoSetor, double salarioBase,
			double imposto, double ajudaDeCusto) {
		super(nome, endereco, telefone, codigoSetor, salarioBase, imposto);
		this.ajudaDeCusto = ajudaDeCusto;
	}
	
	public double getAjudaDeCusto() {
		return ajudaDeCusto;
	}
	public void setAjudaDeCusto(double ajudaDeCusto) {
		this.ajudaDeCusto = ajudaDeCusto;
	}
	public void calcularSalario(double resultado) {
		super.calcularSalario(resultado+ajudaDeCusto);
		
	}
	@Override
	public String toString() {
		return "Administrador [ajudaDeCusto=" + ajudaDeCusto + ", CodigoSetor=" + getCodigoSetor()
				+ ", SalarioBase=" + getSalarioBase() + ", Imposto=" + getImposto() + ", Nome="
				+ getNome() + ", Endereco=" + getEndereco() + ", Telefone=" + getTelefone() + "]";
	}
	
}
