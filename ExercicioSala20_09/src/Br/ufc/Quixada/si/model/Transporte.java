package Br.ufc.Quixada.si.model;

public class Transporte {
	private int capacidade;

	
	public Transporte(int capacidade) {
		this.capacidade = capacidade;
	}
	public int getCapacidade() {
		return capacidade;
	}
	public void setCapacidade(int capacidade) {
		this.capacidade = capacidade;
	}
	
}
