package Br.ufc.Quixada.si.model;

public class Automovel extends Terrestre {
	private String cor;
	private int numPortas;
	private String Placa;
	
	
	public Automovel(int capacidade, int numRodas, String cor, int numPortas, String placa) {
		super(capacidade, numRodas);
		this.cor = cor;
		this.numPortas = numPortas;
		Placa = placa;
	}
	public String getCor() {
		return cor;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	public int getNumPortas() {
		return numPortas;
	}
	public void setNumPortas(int numPortas) {
		this.numPortas = numPortas;
	}
	public String getPlaca() {
		return Placa;
	}
	public void setPlaca(String placa) {
		Placa = placa;
	}
	
}
