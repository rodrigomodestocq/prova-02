package Br.ufc.Quixada.si.model;

public class PessoaJuridica extends Pessoa {
	private String cnpj;
	private int insc_estad;
	private String razao_social;
	public PessoaJuridica(String nome, String endere�o, String email, String cnpj, int insc_estad, String razao_social) {
		super(nome, endere�o, email);
		this.cnpj = cnpj;
		this.insc_estad = insc_estad;
		this.razao_social = razao_social;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public int getInsc_estad() {
		return insc_estad;
	}
	public void setInsc_estad(int insc_estad) {
		this.insc_estad = insc_estad;
	}
	public String getRazao_social() {
		return razao_social;
	}
	public void setRazao_social(String razao_social) {
		this.razao_social = razao_social;
	}
		
	
}
