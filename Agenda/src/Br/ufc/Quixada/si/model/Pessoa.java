package Br.ufc.Quixada.si.model;

public class Pessoa {

	private String nome;
	private String enderešo;
	private String email;
	public Pessoa(String nome, String enderešo, String email) {
		this.nome = nome;
		this.enderešo = enderešo;
		this.email = email;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEnderešo() {
		return enderešo;
	}
	public void setEnderešo(String enderešo) {
		this.enderešo = enderešo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}

