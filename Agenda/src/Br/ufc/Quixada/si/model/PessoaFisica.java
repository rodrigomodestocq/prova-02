package Br.ufc.Quixada.si.model;
import java.time.LocalDate;

public class PessoaFisica extends Pessoa {

	private String cpf;
	private LocalDate Data_Nasc;
	private String estado_civil;
	public PessoaFisica(String nome, String endere�o, String email, LocalDate data_Nasc, String estado_civil, String cpf) {
		super(nome, endere�o, email);
		this.cpf = cpf;
		this.Data_Nasc = data_Nasc;
		this.estado_civil = estado_civil;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public LocalDate getData_Nasc() {
		return Data_Nasc;
	}
	public void setData_Nasc(LocalDate data_Nasc) {
		Data_Nasc = data_Nasc;
	}
	public String getEstado_civil() {
		return estado_civil;
	}
	public void setEstado_civil(String estado_civil) {
		this.estado_civil = estado_civil;
	}
	
	
	
}
