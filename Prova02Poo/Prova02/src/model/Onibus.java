package model;
import java.util.ArrayList;

import Interface.Imprimivel;

public class Onibus implements Imprimivel {
	private int codOnibus;
	private Motorista motorista;
	private ArrayList <Passagem> passagens;
	private ArrayList <Cliente> passageiros;


	public Onibus() {

	}


	public Onibus(int codOnibus, Motorista motorista, ArrayList<Passagem> passagens, ArrayList<Cliente> passageiros) {
		super();
		this.codOnibus = codOnibus;
		this.motorista = motorista;
		this.passagens = passagens;
		this.passageiros = passageiros;
	}
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
	void adicionarPassagemOnibus(Passagem p){
		if(passagens.size()<=63) {
			passageiros.add(p.getCliente());
		}else {
			System.out.println("Onibus lotado!");
		}
	}


	public int getCodOnibus() {
		return codOnibus;
	}


	public void setCodOnibus(int codOnibus) {
		this.codOnibus = codOnibus;
	}


	public Motorista getMotorista() {
		return motorista;
	}


	public void setMotorista(Motorista motorista) {
		this.motorista = motorista;
	}


	public ArrayList<Passagem> getPassagens() {
		return passagens;
	}


	public void setPassagens(ArrayList<Passagem> passagens) {
		this.passagens = passagens;
	}


	public ArrayList<Cliente> getClientes() {
		return passageiros;
	}


	public void setClientes(ArrayList<Cliente> passageiros) {
		this.passageiros = passageiros;
	}


	@Override
	public String toString() {
		return "Onibus [codOnibus=" + codOnibus + ", motorista=" + motorista + ", passagens=" + passagens
				+ ", passageiros=" + passageiros + "]";
	}


	@Override
	public void mostrarPassagens() {
		passagens.toString();
	}	
}

