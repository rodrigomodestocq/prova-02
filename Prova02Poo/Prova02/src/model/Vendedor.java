package model;

public class Vendedor extends Funcionario {
	private	int cargaHoraria;

	public Vendedor(String nome, String cpf, String matricula, float salario, int cargaHoraria) {
		super(nome, cpf, matricula, salario);
		this.cargaHoraria = cargaHoraria;
	}
	void realizarVendas(Onibus o, Passagem p){
		o.adicionarPassagemOnibus(p);
		DarBonificacao();
	}

	public int getCargaHoraria() {
		return cargaHoraria;
	}
	public void setCargaHoraria(int cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}

	@Override
	public String toString() {
		return "Vendedor [Cpf=" + getCpf() + ", Matricula=" + getMatricula() + ", Salario="
				+ getSalario() + ", Nome=" + getNome() + ", cargaHoraria=" + cargaHoraria + "]";
	}
	@Override
	void DarBonificacao() {
		setSalario(getSalario() + 3); 
	}
}
