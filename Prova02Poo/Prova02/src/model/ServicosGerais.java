package model;

public class ServicosGerais extends Funcionario {
	
	private int tempoServico;

	public ServicosGerais(String nome, String cpf, String matricula, float salario, int tempoServico) {
		super(nome, cpf, matricula, salario);
		this.tempoServico = tempoServico;
	}
	void limpar() {
		DarBonificacao();
	}
	public int getTempoServico() {
		return tempoServico;
	}
	public void setTempoServico(int tempoServico) {
		this.tempoServico = tempoServico;
	}
	@Override
	public String toString() {
		return "ServicosGerais [Cpf=" + getCpf() + ", Matricula=" + getMatricula() + ", Salario()="
				+ getSalario() + ", Nome=" + getNome() + ", tempoServico=" + tempoServico + "]";
	}
	@Override
	void DarBonificacao() {
		setSalario(getSalario() + 3); 
		
	}
	
	
	
}
