package model;

public class Motorista extends Funcionario {
	private String cnh;
	
	public Motorista(String nome, String cpf, String matricula, float salario, String cnh) {
		super(nome, cpf, matricula, salario);
		this.cnh = cnh;
	}

	void realizarViagem() {

	}

	public String getCnh() {
		return cnh;
	}
	public void setCnh(String cnh) {
		this.cnh = cnh;
	}

	@Override
	public String toString() {
		return "Motorista [Cpf=" + getCpf() + ", Matricula=" + getMatricula() + ", Salario="
				+ getSalario() + ", Nome=" + getNome() + ", cnh=" + cnh + "]";
	}

	@Override
	void DarBonificacao() {
		setSalario(getSalario() + 1.05f); 
		
	}	
}

